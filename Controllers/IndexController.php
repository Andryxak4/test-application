<?php

/**
 * Manages URL requests '/index/*' or '/' (while default controller is 'index')
 */
class IndexController extends BaseController {

    /**
     * Directory name used for controller's template files
     * @var string
     */
    protected $view_template_directory = 'index';

    /**
     * Displays 'lorem ipsum' text
     */
    public function actionIndex() {
        $this->render('index');
    }

}
