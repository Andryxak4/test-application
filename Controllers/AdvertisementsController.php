<?php

/**
 * Manages URL requests '/advertisements/*'
 */
class AdvertisementsController extends BaseController {

    /**
     * Directory name used for controller's template files
     * @var string
     */
    protected $view_template_directory = 'advertisements';

    /**
     * Displays advertisements table with users names
     * Accessible with URL request '/advertisements/index/'
     * or '/advertisements/' (while default action is 'index')
     */
    public function actionIndex() {
        $advertisement = new AdvertisementModel;
        $all_advertisements = $advertisement->getList();
        $this->active_menu_item = 'advertisements';
        $this->render('index', array('all_advertisements' => $all_advertisements));
    }

}
