<?php

/**
 * Manages URL requests '/users/*'
 */
class UsersController extends BaseController {

    /**
     * Directory name used for controller's template files
     * @var string
     */
    protected $view_template_directory = 'users';

    /**
     * Displays users table
     * Accessible with URL request '/users/index/'
     * or '/users/' (while default action is 'index')
     */
    public function actionIndex() {
        $user = new UserModel;
        $all_users = $user->findAll();
        $this->active_menu_item = 'users';
        $this->render('index', array('all_users' => $all_users));
    }

}
