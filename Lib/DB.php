<?php

/**
 * Singleton class that provides access to database
 */
class DB {

    /**
     * Object which represents the connection to a MySQL Server
     * @var mysqli 
     */
    public static $link;
    /**
     * Singleton instance object
     * @var DB
     */
    private static $instance;

    /**
     * Singleton private constructor
     * @global array $config
     * @throws Exception if DB connection settings weren't found in config.php
     * or if there was some DB connection error
     */
    private function __construct() {
        global $config;
        if (!isset($config['db_host']) || !isset($config['db_user']) || !isset($config['db_password']) || !isset($config['db_name'])) {
            throw new Exception("Check DB connection settings in config.php! Required fields: db_host, db_user, db_password, db_name");
        }
        self::$link = new mysqli($config['db_host'], $config['db_user'], $config['db_password'], $config['db_name']);
        if (self::$link->connect_error) {
            throw new Exception('Connect Error (' . self::$link->connect_errno . ') ' . self::$link->connect_error);
        }
    }

    /**
     * To prevent singleton object cloning
     */
    private function __clone() {
        
    }

    /**
     * To prevent singleton object creating by 'unserialize'
     */
    private function __wakeup() {
        
    }

    /**
     * Returns singleton instance object
     * @return DB
     */
    public static function db() {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Closes DB connection
     */
    public function __destruct() {
        self::$link->close();
    }

    /**
     * Returns SQL query result
     * @param string $query The query string
     * @return mixed Returns FALSE on failure.
     * For successful SELECT, SHOW, DESCRIBE or EXPLAIN queries will return
     * a mysqli_result object. For other successful queries will return TRUE.
     */
    public function query($query) {
        return self::$link->query($query);
    }

    /**
     * Returns array with SQL query result rows
     * @param string $query The query string
     * @return array Array of associative arrays representing each row in query result
     */
    public function getRows($query) {
        $query_result = self::$link->query($query);
        $result = array();
        while ($row = mysqli_fetch_assoc($query_result)) {
            $result[] = $row;
        }
        return $result;
    }

}
