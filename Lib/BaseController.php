<?php

/**
 * Base controller class
 */
class BaseController {

    /**
     * Name of default action to execute
     * @var string
     */
    protected static $default_action;

    /**
     * View object
     * @var View
     */
    protected static $view;

    /**
     * Key of active menu item (in view template)
     * @var string
     */
    protected $active_menu_item = 'index';

    /**
     * Directory name used for controller's template files
     * @var string
     */
    protected $view_template_directory;

    /**
     * Creates View object
     */
    public function __construct() {
        static::$view = new View();
    }

    /**
     * Renders a view using template file $template
     * @param array $template name of template file without '.php' ending<br>
     * Template files is located in directory '/View/<controller_name>/',
     * and <controller_name> is equal to value of 'view_template_directory' property
     * of controller object
     * @param array $vars Array of variables that can be accessed in template file
     */
    public function render($template, $vars = array()) {
        static::$view->template = ROOT . '/View/' . $this->view_template_directory . '/' . $template . '.php';
        $default_params = array(
            'menu' => $this->getMenu(),
            'active_menu_item' => $this->active_menu_item,
        );
        static::$view->display(array_merge($default_params, $vars));
    }

    /**
     * Returns menu items array (for view template)
     * @global array $config
     * @return array Array with menu items, each of them is array with two elements:
     * 'url' - menu item URL;
     * 'title' - menu item text
     * @throws Exception if menu settings weren't found in config.php
     */
    public function getMenu() {
        global $config;
        if (!isset($config['menu'])) {
            throw new Exception("Menu is not set");
        }
        return $config['menu'];
    }

}
