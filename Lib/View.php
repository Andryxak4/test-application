<?php

/**
 * View class
 */
class View {

    /**
     * Full path to view file
     * @var string 
     */
    public $template;

    /**
     * Loads $template file and extracts given variables for it
     * @param array $vars Array of variables that can be accessed in template file
     * @throws Exception if $template file doesn't exists
     * or if $vars isn't array
     */
    public function display($vars = array()) {
        if (!is_array($vars)) {
            throw new Exception("Invalid argument type passed");
        }
        extract($vars);
        if (!file_exists($this->template)) {
            throw new Exception("File <b>{$this->template}</b> doesn't exists");
        }
        include_once ROOT . '/View/layout/header.php';
        include_once $this->template;
        include_once ROOT . '/View/layout/footer.php';
    }

}
