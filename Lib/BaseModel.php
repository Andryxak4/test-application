<?php

/**
 * Base model class
 * Is used for working with single database table
 */
class BaseModel {

    const COUNT_LIMIT = 20;

    /**
     * Object that provides access to database
     * @var DB
     */
    protected $db;

    /**
     * Table name
     * @var string
     */
    public static $table;

    /**
     * Table columns list
     * @var array 
     */
    public static $fields_list;

    public function __construct() {
        $this->db = DB::db();
    }

    /**
     * Returns rows from database table
     * @param integer $offset Number of rows from beginning to skip
     * @param integer $limit Max number of rows to return
     * @return array Array with selected rows
     * @throws Exception if $offset or $limit isn't non-negative integer
     */
    public function findAll($offset = 0, $limit = self::COUNT_LIMIT) {
        if (!is_numeric($offset) || !is_numeric($limit) || $offset < 0 || $limit < 0) {
            throw new Exception("Invalid argument type passed");
        }
        $sql = "SELECT * FROM `" . static::$table . "` LIMIT $offset, $limit";
        return $this->db->getRows($sql);
    }

}
