<?php

/**
 * Determines which controller and action should be called
 * according to URL
 */
class Router {

    /**
     * Determines and calls controller and action according to URL
     * @param string $path requested URL
     */
    public static function run($path = '') {
        $path_components = explode("/", $path);
        $controller_name = (isset($path_components[0]) && !empty($path_components[0])) ? $path_components[0] : // passed in URL
                self::getDefaultControllerName(); // default from config
        $controller_object = self::getControllerObject($controller_name);

        $action_name = (isset($path_components[1]) && !empty($path_components[1])) ?
                $path_components[1] : // passed in URL
                (property_exists($controller_object, "default_action") && !empty($controller_object::$default_action) ?
                        $controller_object::$default_action : // default for this controller
                        self::getDefaultActionName()); // default from config

        $action_method_name = self::getActionMethodName($controller_object, $action_name);

        call_user_func(array($controller_object, $action_method_name));
    }

    /**
     * Determines required controller class, includes it, creates
     * and returns controller object according to URL or default settings from config
     * @param string $controller_name Required controller name
     * @return mixed Object of some controller class
     * @throws Exception if required controller class wasn't found
     */
    private static function getControllerObject($controller_name) {
        $path_to_controller = ROOT . "/Controllers/" . ucfirst($controller_name) . "Controller.php";
        if (!file_exists($path_to_controller)) {
            throw new Exception("File <b>$path_to_controller</b> doesn't exists!");
        }

        require_once $path_to_controller;
        $controller_class_name = ucfirst($controller_name) . "Controller";

        if (!class_exists($controller_class_name)) {
            throw new Exception("Class <b>$controller_class_name</b> isn't declared!");
        }

        return new $controller_class_name;
    }

    /**
     * Determines method name for required action for passed controller object
     * @param mixed $controller_object Object of some controller class, determined earlier
     * @param string $action_name required action,
     * determined according to URL or controller defaults, or default settings from config
     * @return string Determined method name
     * @throws Exception if method for required action wasn't found
     */
    private static function getActionMethodName($controller_object, $action_name) {
        $controller_class_name = get_class($controller_object);
        $action_method_name = "action" . ucfirst($action_name);
        if (!method_exists($controller_object, $action_method_name)) {
            throw new Exception("Method <b>$action_method_name</b> isn't declared in <b>$controller_class_name</b>!");
        } else {
            return $action_method_name;
        }
    }

    /**
     * Returns name of default controller from application config
     * @global array $config application config array
     * @return string default controller name  from application config
     * @throws Exception if 'default_controller' field isn't set in config array
     */
    public static function getDefaultControllerName() {
        global $config;
        if (!isset($config['default_controller'])) {
            throw new Exception("Default controller isn't set!");
        }
        return $config['default_controller'];
    }

    /**
     * Returns name of default action from application config
     * @global array $config application config array
     * @return string default action name from application config
     * @throws Exception if 'default_action' field isn't set in config array
     */
    public static function getDefaultActionName() {
        global $config;
        if (!isset($config['default_action'])) {
            throw new Exception("Default action isn't set!");
        }
        return $config['default_action'];
    }

}

?>