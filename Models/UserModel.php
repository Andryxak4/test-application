<?php

/**
 * Class for working with 'users' database table
 */
class UserModel extends BaseModel {

    /**
     * Database table name
     * @var string 
     */
    public static $table = 'users';

    /**
     * Array containing table columns names
     * @var array $fields_list 
     */
    public static $fields_list = array('id', 'name');

}
