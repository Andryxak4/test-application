<?php

/**
 * Class for working with 'advertisements' database table
 */
class AdvertisementModel extends BaseModel {

    /**
     * Database table name
     * @var string 
     */
    public static $table = 'advertisements';

    /**
     * Array containing table columns names
     * @var array $fields_list 
     */
    public static $fields_list = array('id', 'user_id', 'title');

    /**
     * Returns advertisements list 
     * @return array an associative array of rows with columns 'id', 'name', 'title',<br>
     * where 'id' - advertisement's id, 'name' - related user's name,
     * 'title' - advertisement's title
     */
    public function getList() {
        $all_advertisements = $this->db->query("SELECT a.id AS id, u.name AS name, a.title AS title"
                . " FROM " . self::$table . " AS a"
                . " INNER JOIN " . UserModel::$table . " AS u ON a.user_id=u.id");
        $result = array();
        while ($row = mysqli_fetch_assoc($all_advertisements)) {
            $result[] = $row;
        }
        return $result;
    }

}
