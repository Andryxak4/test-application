<div class='content'>
    <?php if ($all_advertisements && is_array($all_advertisements)) { ?>
        <table border=1>
            <thead>
                <tr><th><?= implode("</th>\n<th>", array_keys($all_advertisements[0])); ?></th></tr>
            </thead>
            <tbody>
                <?php foreach ($all_advertisements as $row) { ?>
                    <tr>
                        <td><?= implode("</td><td>",$row); ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } else { ?>
        <p>No results found!</p>
    <?php } ?>
</div>