<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>Simple Application</title>
        <link rel="stylesheet" type="text/css" href='/style.css'/>
    </head>
    <body>
        <header>
            <div class='nav'>
                <ul class="nav-menu">
                    <?php
                    if (isset($menu) && is_array($menu) && $menu) {
                        foreach ($menu as $key => $menu_item) {
                            $active = (isset($active_menu_item) && !empty($active_menu_item) && $active_menu_item == $key) ? 'active' : '';
                            echo "<li class='menu-item $active'>\n"
                            . "<a href='{$menu_item['url']}'>{$menu_item['title']}</a>\n"
                            . "</li>\n";
                        }
                    }
                    ?>
                </ul>
            </div>
        </header>
        <div class="page">
        <?php
            if (isset($menu) && is_array($menu) && 
                    isset($active_menu_item) && !empty($active_menu_item) && 
                    isset($menu[$active_menu_item]) && !empty($menu[$active_menu_item]['title'])) {
                echo "<h1>".$menu[$active_menu_item]['title']."</h1>";
            }