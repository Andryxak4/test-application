<?php

try {
    define('ROOT', $_SERVER['DOCUMENT_ROOT']);
    /* loads application config */
    $config = require_once ROOT . '/config.php';
    if (isset($config['debug']) && $config['debug']) {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
    } else {
        ini_set('display_errors', 0);
    }

    /* $_GET['path'] is set because of .htaccess config */
    $path = isset($_GET['path']) ? $_GET['path'] : '';

    /* includes all files from 'Lib' directory */
    foreach (glob(ROOT . "/Lib/*.php") as $filename) {
        include $filename;
    }
    /* includes all files from 'Models' directory */
    foreach (glob(ROOT . "/Models/*.php") as $filename) {
        include $filename;
    }

    /* Runs URL processing */
    Router::run($path);
} catch (Exception $e) {
    echo "<pre>";
    echo $e->getMessage() . "\n";
    echo "\nStack trace:\n";
    echo $e->getTraceAsString() . "\n";
    echo "</pre>";
}
?>