<?php

return array(
    'db_host' => 'localhost',
    'db_user' => 'sa',
    'db_password' => 'yjDZrHPI',
    'db_name' => 'andre_k4_sa',
    'default_controller' => 'index',
    'default_action' => 'index',
    'debug' => false,
    'menu' => array(
        'index' => array(
            'url' => '/',
            'title' => 'Home'
        ),
        'users' => array(
            'url' => '/users',
            'title' => 'Users List'
        ),
        'advertisements' => array(
            'url' => '/advertisements',
            'title' => 'Advertisements List'
        )
    ),
);
